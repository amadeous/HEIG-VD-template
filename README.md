Ce dépôt vise à partager des templates et différents documents pour la
rédaction de rapports ou la préparation de présentations pour les
collaborateurs et les étudiants de la HEIG-VD.
Pour l'instant il contient un template LaTeX pour éditer des présentations
aux couleurs officielles de l'école.

Toute contribution sera bienvenue, n'hésitez pas à participer.

La maintenance est actuellement assurée par Yann Thoma et Florian Dufour.

